---
layout: handbook-page-toc
title:  Gitlab Laptop Delivery Metrics
---
## On this page
{:.no_toc .hidden-md .hidden-lg}
- TOC
{:toc .hidden-md .hidden-lg}

## Gitlab Laptop Delivery Metrics

### Purpose

We're desiging this page to publicly and transparently show our metrics around laptop shipping, delivery, and procurement. This page is in its early stage and we will iterate and add more content as we continue to advance our metrics. 

### Table Breakdown

The **Regions** section is detailing what region and what metric we are tracking, within we are reporting total laptops delivered per region and percentage of on time delivery for the related month. The acronym ROW stands for **Rest of the World**, these are region that we hire in but do not have a registered vendor or shipping entity. Please note these numbers do not reflect new hires or team members that opt to self procure their own laptop. 


 
| Regions                         | August  | September | October | November | December | January | February |
| -------------                   |:-------:|:---------:|:-------:|:--------:|:--------:|:--------|:---------|
| EMEA Laptops delivered          |20       |17         |16       |20        |9         |6        |5         |
| EMEA % on time                  |90%      |94.12%     |94.12%   |90%       |99.90%    |83.40%   |100%      |
| North America laptops delivered |63       |42         |44       |29        |18        |7        |13        |
| North America % on time         |87.31%   |92.86%     |90.91%   |100%      |83.49%    |100%     |84.70%    |
| APAC Laptops delivered          |19       |7          |9        |4         |1         |5        |2         |
| APAC % on time                  |94.74%   |100%       |55.60%   |100%      |100%      |80%      |100%      |
| LATAM Laptops delivered         |0        |0          |0        |0         |0         |0        |0         |
| LATAM % on time                 |N/A      |N/A        |N/A      |N/A       |N/A       |N/A      |N/A       |
| ROW Laptops delivered           |0        |0          |0        |0         |0         |0        |0         |
| ROW % on time                   |N/A      |N/A        |N/A      |N/A       |N/A       |N/A      |N/A       |
| Total laptops delivered         |102      |68         |69       |53        |29        |18       |20        |
| % Laptops delivered on time     |88.24%   |92.65%     |85.30%   |96.23%    |86.30%    |88.90%   |90%       |

